# Goals 
To have fun and learn some racekt
## MVP

1. ~~Add a bitmap to the canvas using the file browser and the open menu-item~~

2. Text-Box to add text to image wherever it's clicked
  a. Ability to change fonts
  b. Ability to set color of text
  c. Ability to change size of font

3. Draw on an image using a pencil tool that follows the mouse
  a. ~~Toggle pencil tool using a button~~
 b. ~~Toggle pencil thickness using a text field~~
  c. ~~Toggle pencil color using a button~~
  d. Toggle pencil style using a button? (I don't know how I feel about this yet)

4. Save picture

5. New file button

6. Eraser button
  a. ~~get the canvas background~~
  b. ~~set the "eraser" to the canvas background~~
  c. ~~save erase to a separate list~~ I used the same hash
  d. ~~always iterate through this last so it "paints over" everything~~ Found a better way
  e. if the background changes set the eraser to the new background color (not possible to change background yet)

7. Clear screen button?
## Release Ready
1. Ability to read gifs
2. Carosel at bottom of program for quickly scrolling through individual images in gifs
3. Tool to easily interpolate text or other bitmaps between individual images in gifs
5. Rectangle creator button
6. Circle creator button
7. Selection button
8. Color Palette
9. Line button
10. Squiggly Line button
11. Crop
12. Library of commonly added accoutrements to memes (I.E Deal with it glasses, Red Eye Beam, Scum bag steve hat) that allows for easy placement


## TODO 
1. ~~Fix the bitmap being erased from the canvas due to the canvas refreshing while drawing~~
2. Start breaking out the main file into smaller files
3. ~~Restrict the text-field to just numbers~~
   a. ~~Remove changing pencil thickness only when you hit enter in the text field~~
5. change the toolbar to be horizontal 
7. ~~Toggle pencil color using a button~~
  a. ~~Creating a button with a color representing foreground~~
    aa. clicking the button will open up a dialog? with colors from colors built in
  b. Creating a button with a color represeting the background
    ba. clicking the button will open up a dialog? with colors from colors built in
8. Toggle pencil style using a button?
9. Save my beautiful art to a png
~~10. Make the new colors draw on top of the old colors~~ 
11. Implement Eraser tool
  a. ~~get the canvas background~~
  b. ~~set the "eraser" to the canvas background~~
  c. if the background changes set the eraser to the new background color
  
## Questions
Can I use the https://docs.racket-lang.org/draw/dc___.html#%28meth._%28%28%28lib._racket%2Fdraw..rkt%29._dc~3c~25~3e%29._draw-bitmap%29%29 draw-bitmap-section as a crop?
